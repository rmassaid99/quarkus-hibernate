package org.acme.model;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.List;

@Entity
public class Movies extends PanacheEntityBase {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", length = 36, nullable = false)
    public String id;

    @Column(length = 40)
    public String title;


    @Column(length = 5)
    public String release_year;

    public String description;

    @Column(length =40)
    public String genre;

    public Double rating;

    public static List<Movies> findById(String id) {
        return find("id", id).list();
    }
}


