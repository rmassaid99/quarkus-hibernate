package org.acme.controler;

import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.vertx.core.json.JsonObject;
import org.acme.model.Movies;

import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/movies")
public class moviesControler {
    @GET
    public Response list() {
        PanacheQuery<Movies> moviesList = Movies.findAll();
        List<Movies> movies = moviesList.list();
        return Response.ok().entity(movies).build();
    }

    @GET
    @Path("/{id}")
    public Response moviedetail(@PathParam("id") String id) {
        List<Movies> movie = Movies.findById(id);
        return Response.ok(movie).build();
    }

    @POST
    @Transactional
    public Movies list(JsonObject payload) {
        Movies movie = new Movies();
        movie.title = payload.getString("title");
        movie.description = payload.getString("description");
        movie.genre=payload.getString("genre");
        movie.release_year=payload.getString("release_year");
        movie.rating= payload.getDouble("rating");
        movie.persist();
        return movie;
    }

    @DELETE
    @Transactional
    public JsonObject deleteMovies(@QueryParam("title") String title) {
        Movies.delete("title = ?1",title);
        return new JsonObject();
    }
    @PUT
    @Path("/{id}")
    @Transactional
    public JsonObject updateMovies(@PathParam("id") String id, @QueryParam("title") String title,
                             @QueryParam("description") String description, @QueryParam("genre") String genre,
                                   @QueryParam("rating") Double rating, @QueryParam("release_year") String release_year) {


        Movies.update("description = ?1, title = ?2 , genre = ?3, rating = ?4, release_year = ?5 where id = ?6",
                description, title, genre, rating, release_year ,id);
        return new JsonObject();
    }

    @PATCH
    @Transactional
    public JsonObject updateMovies(@QueryParam("oldTitle") String oldTitle, @QueryParam("newTitle") String newTitle) {
        Movies.update("title = ?1 where title= ?2", newTitle, oldTitle);
        return new JsonObject();
    }


}
